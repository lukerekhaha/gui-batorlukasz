package introduction;

import java.awt.EventQueue;
import javax.swing.JFrame;

public class Introduction extends JFrame {

    public Introduction() {

        initUI();
    }

    private void initUI() {
        
        setTitle("Simple example");
        setSize(300, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {
        
            @Override
            public void run() {
                Introduction ex = new Introduction();
                ex.setVisible(true);
            }
        });
    }
}