/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PaintPackage;

import java.awt.Color;
import static java.awt.Color.*;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.imageio.ImageIO;

/**
 *
 * @author Luke
 */
public class Glowny extends javax.swing.JFrame implements MouseMotionListener, MouseListener {

    /**
     * Creates new form Glowny
     */
    private Color color = Color.black;
    private Point stary, obecny, a;
    private int grubosc = 5;
    private Image obraz;
    private BufferedImage image;
    
    public Glowny() {
        initComponents();
        PanelRysownia.setVisible(true);
        PanelRysownia.addMouseMotionListener(this);
        PanelRysownia.addMouseListener(this);
    }
    /*@Override
    public void paint(Graphics g)
    {
        if (obraz != null) {
            g.drawImage(obraz, 0, 0, PanelRysownia);
        } else {
            obraz = createImage(PanelRysownia.getWidth(), PanelRysownia.getHeight());
        }
    }
    
    protected void paintComponent(Graphics g){
        if(obraz == null){
            obraz = PanelRysownia.createImage(getSize().width, getSize().height);
            g2 = (Graphics2D) obraz.getGraphics();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            repaint(); //oczyszczenie pola rysowania
        }
        g.drawImage(obraz, 0, 0, null);
    }
    */
    @Override
    public void mouseDragged(MouseEvent x) {
        obecny = x.getPoint();

        if (ButtonOlowek.isSelected()) {
            Graphics g = PanelRysownia.getGraphics();
            g.setColor(color);
            g.drawLine(stary.x, stary.y, obecny.x, obecny.y);
            //repaint();
            stary = obecny;
            
        }
        if (ButtonGumka.isSelected()) {
            Graphics g = PanelRysownia.getGraphics();
            //g.setColor(black);
            //g.drawRect(obecny.x, obecny.y, grubosc, grubosc);
            g.setColor(white);
            g.fillOval(obecny.x, obecny.y, grubosc, grubosc);
        }

    }
    
    @Override
    public void mouseClicked(MouseEvent x) {

        if (ButtonGumka.isSelected()) {
            obecny = x.getPoint();
            Graphics g = PanelRysownia.getGraphics();
            //g.setColor(black);
            //g.drawRect(obecny.x, obecny.y, grubosc, grubosc);
            g.setColor(white);
            g.fillOval(obecny.x, obecny.y, grubosc, grubosc);
        }
    }
    
    @Override
    public void mousePressed(MouseEvent x) {
        stary = x.getPoint();
        
        if (ButtonOlowek.isSelected()) {
            stary = x.getPoint();
        }
        if (ButtonLinia.isSelected()) {
            obecny = x.getPoint();
            Graphics g = PanelRysownia.getGraphics();
            g.setColor(color);
            g.drawLine(obecny.x, obecny.y, obecny.x, obecny.y);
        }
        if (ButtonElipsa.isSelected()) {
            obecny = x.getPoint();
            Graphics g = PanelRysownia.getGraphics();
            g.setColor(color);
        }
        if (ButtonKwadrat.isSelected()) {
            obecny = x.getPoint();
            Graphics g = PanelRysownia.getGraphics();
            g.setColor(color);
            g.drawRect(obecny.x, obecny.y, 1, 1);
        }
    }
    
    @Override
    public void mouseReleased(MouseEvent x) {
        if (ButtonLinia.isSelected()) {
            a = x.getPoint();
            Graphics g = PanelRysownia.getGraphics();
            g.setColor(color);
            g.drawLine(stary.x, stary.y, a.x, a.y);
        }

        if (ButtonElipsa.isSelected()) {
            a = x.getPoint();
            Graphics g = PanelRysownia.getGraphics();
            g.setColor(color);
            g.drawOval(stary.x, stary.y, a.x - stary.x, a.y - stary.y);
        }

        if (ButtonKwadrat.isSelected()) {
            a = x.getPoint();
            Graphics g = PanelRysownia.getGraphics();
            g.setColor(color);
            g.drawRect(stary.x, stary.y, a.x - stary.x, a.y - stary.y);
        }

    }
    
    @Override
    public void mouseMoved(MouseEvent me) {

    }
    
    @Override
    public void mouseExited(MouseEvent me) {

    }
    
    @Override
    public void mouseEntered(MouseEvent me) {

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialogKolory = new javax.swing.JDialog();
        Barwy = new javax.swing.JColorChooser();
        jButtonOk = new javax.swing.JButton();
        jButtonAnuluj = new javax.swing.JButton();
        buttonGroup1 = new javax.swing.ButtonGroup();
        PanelOpcje = new javax.swing.JPanel();
        ButtonKolory = new javax.swing.JButton();
        jSlider1 = new javax.swing.JSlider();
        ButtonOlowek = new javax.swing.JToggleButton();
        ButtonLinia = new javax.swing.JToggleButton();
        ButtonKwadrat = new javax.swing.JToggleButton();
        ButtonElipsa = new javax.swing.JToggleButton();
        ButtonGumka = new javax.swing.JToggleButton();
        PanelRysownia = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();

        jDialogKolory.setMinimumSize(new java.awt.Dimension(612, 450));
        jDialogKolory.setSize(new java.awt.Dimension(520, 400));

        jButtonOk.setText("OK");
        jButtonOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOkActionPerformed(evt);
            }
        });

        jButtonAnuluj.setText("Anuluj");
        jButtonAnuluj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAnulujActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialogKoloryLayout = new javax.swing.GroupLayout(jDialogKolory.getContentPane());
        jDialogKolory.getContentPane().setLayout(jDialogKoloryLayout);
        jDialogKoloryLayout.setHorizontalGroup(
            jDialogKoloryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialogKoloryLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jDialogKoloryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialogKoloryLayout.createSequentialGroup()
                        .addComponent(jButtonOk)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonAnuluj))
                    .addComponent(Barwy, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 495, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jDialogKoloryLayout.setVerticalGroup(
            jDialogKoloryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialogKoloryLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Barwy, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jDialogKoloryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonOk)
                    .addComponent(jButtonAnuluj))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        PanelOpcje.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        ButtonKolory.setBackground(new java.awt.Color(102, 255, 204));
        ButtonKolory.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        ButtonKolory.setText("Kolor");
        ButtonKolory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonKoloryActionPerformed(evt);
            }
        });

        jSlider1.setMajorTickSpacing(5);
        jSlider1.setMaximum(30);
        jSlider1.setOrientation(javax.swing.JSlider.VERTICAL);
        jSlider1.setPaintLabels(true);
        jSlider1.setValue(5);
        jSlider1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSlider1StateChanged(evt);
            }
        });

        buttonGroup1.add(ButtonOlowek);
        ButtonOlowek.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        ButtonOlowek.setText("Ołówek");

        buttonGroup1.add(ButtonLinia);
        ButtonLinia.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        ButtonLinia.setText("Linia");

        buttonGroup1.add(ButtonKwadrat);
        ButtonKwadrat.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        ButtonKwadrat.setText("Kwadrat");

        buttonGroup1.add(ButtonElipsa);
        ButtonElipsa.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        ButtonElipsa.setText("Elipsa");

        buttonGroup1.add(ButtonGumka);
        ButtonGumka.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        ButtonGumka.setText("Gumka");
        ButtonGumka.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonGumkaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelOpcjeLayout = new javax.swing.GroupLayout(PanelOpcje);
        PanelOpcje.setLayout(PanelOpcjeLayout);
        PanelOpcjeLayout.setHorizontalGroup(
            PanelOpcjeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelOpcjeLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(PanelOpcjeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelOpcjeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(ButtonLinia, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ButtonOlowek, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelOpcjeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(ButtonElipsa, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ButtonKwadrat, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ButtonGumka, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(ButtonKolory))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        PanelOpcjeLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {ButtonElipsa, ButtonGumka, ButtonKwadrat, ButtonLinia, ButtonOlowek});

        PanelOpcjeLayout.setVerticalGroup(
            PanelOpcjeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelOpcjeLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(PanelOpcjeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(PanelOpcjeLayout.createSequentialGroup()
                        .addComponent(ButtonOlowek)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ButtonLinia)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ButtonKwadrat)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ButtonElipsa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ButtonGumka)
                        .addGap(26, 26, 26)
                        .addComponent(ButtonKolory, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        PanelRysownia.setBackground(new java.awt.Color(255, 255, 255));
        PanelRysownia.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout PanelRysowniaLayout = new javax.swing.GroupLayout(PanelRysownia);
        PanelRysownia.setLayout(PanelRysowniaLayout);
        PanelRysowniaLayout.setHorizontalGroup(
            PanelRysowniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 583, Short.MAX_VALUE)
        );
        PanelRysowniaLayout.setVerticalGroup(
            PanelRysowniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PaintPackage/paint.jpg"))); // NOI18N

        jMenu1.setText("Plik");

        jMenuItem1.setText("Nowy");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Zapisz");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem3.setText("Wyjdź");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(PanelOpcje, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PanelRysownia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(PanelOpcje, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(PanelRysownia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        repaint();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        BufferedImage image = new BufferedImage(PanelRysownia.getWidth(), PanelRysownia.getHeight(), BufferedImage.TYPE_INT_RGB);
        image.createGraphics();
        PanelRysownia.paintAll(image.getGraphics());
        try {
            ImageIO.write(image, "jpg", new File("plik.jpg"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
       
        
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jButtonOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOkActionPerformed
        // TODO add your handling code here:
        color = Barwy.getColor();
        jDialogKolory.setVisible(false);
    }//GEN-LAST:event_jButtonOkActionPerformed

    private void jButtonAnulujActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAnulujActionPerformed
        // TODO add your handling code here:
        jDialogKolory.setVisible(false);
    }//GEN-LAST:event_jButtonAnulujActionPerformed

    private void ButtonKoloryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonKoloryActionPerformed
        // TODO add your handling code here:
        jDialogKolory.setVisible(true);
    }//GEN-LAST:event_ButtonKoloryActionPerformed

    private void jSlider1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSlider1StateChanged
        // TODO add your handling code here:
        grubosc = jSlider1.getValue();
    }//GEN-LAST:event_jSlider1StateChanged

    private void ButtonGumkaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonGumkaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ButtonGumkaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Glowny.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Glowny.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Glowny.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Glowny.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Glowny().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JColorChooser Barwy;
    private javax.swing.JToggleButton ButtonElipsa;
    private javax.swing.JToggleButton ButtonGumka;
    private javax.swing.JButton ButtonKolory;
    private javax.swing.JToggleButton ButtonKwadrat;
    private javax.swing.JToggleButton ButtonLinia;
    private javax.swing.JToggleButton ButtonOlowek;
    private javax.swing.JPanel PanelOpcje;
    private javax.swing.JPanel PanelRysownia;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonAnuluj;
    private javax.swing.JButton jButtonOk;
    private javax.swing.JDialog jDialogKolory;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JSlider jSlider1;
    // End of variables declaration//GEN-END:variables
}
